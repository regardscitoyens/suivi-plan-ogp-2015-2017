# Engagement 16 : Favoriser l'ouverture des modèles de calcul et des simulateurs de l'État

## Institutions porteuses : 
- Secrétariat d'État chargé de la Réforme de l'État et de la Simplification

## Enjeux : 
L'ouverture des données appelle rapidement l'ouverture des règles et des modèles qui les produisent, les complètent et fondent la décision publique. Ces algorithmes et modèles sont en effet une puissante ressource pour favoriser des points de vue indépendants, et, quand ils sont ouverts (open source) et maniables à travers des API, pour développer de nombreuses applications.

## Description de l'engagement : 
- **Étendre l'ouverture des modèles à d'autres champs de l'action publique**
    - Continuer à travailler avec les différentes administrations pour les accompagner dans l'ouverture de leurs modèles de calculs et de simulation
- **Produire des simulateurs à partir des modèles ouverts existants**
    - Capitaliser sur la plateforme [OpenFisca](http://www.openfisca.fr/) pour l'étendre à d'autres domaines de la législation et proposer des déclinaisons de simulateurs utiles aux citoyens, aux acteurs économiques et aux acteurs publics : par exemple, un modèle de simulation des coûts énergétiques, une extension à la fiscalité locale, aux calculs de retraite…

[En savoir plus](http://gouvernement-ouvert.etalab.gouv.fr/content/fr/partager-des-ressources/engagement-16.html)
 
## Description des résultats :

**Actions** | **Résultats** | **Prochaines étapes** | **Statut**
--- | --- | --- | ---
Continuer à travailler avec les différentes administrations pour les accompagner dans l'ouverture de leurs modèles de calculs et de simulation. | Le ministère des Finances et des Comptes publics a ouvert le code source de son calculateur des impôts. A cette occasion, un [hackathon a été organisé par la Direction générale des finances publiques et la mission Etalab les 1er et 2 avril](https://www.etalab.gouv.fr/retour-sur-le-hackathon-codeimpot), afin de développer des applications et services concrets à partir du code source et d'encourager les autres ministères à ouvrir leurs codes sources. Cet événément a eu lieu en présence du ministre des Finances et des Comptes publics, du ministre du Budget et de la secrétariat d'Etat chargé du Numérique. | Suivre les résultats du hackathon #CodeImpots. Poursuivre la discussion avec les institutions concernées : des réunions de travail ont notamment débuté avec l'INSEE concernant le simulateur de pensions individuelles. Accompagner l'ouverture du simulateur de la DILA (calculateur de la carte grise) : ouverture prévue en septembre 2016 et organisation d'un hackathon commun DILA/ Etalab sur les simulateurs sur novembre. | Partiel
Capitaliser sur la plateforme [OpenFisca](http://www.openfisca.fr/) pour l'étendre à d'autres domaines de la législation et proposer des déclinaisons de simulateurs utiles aux citoyens, aux acteurs économiques et aux acteurs publics. | Le code source du calculateur impôts est en cours d'intégration dans [OpenFisca](http://www.openfisca.fr/) et la collaboration se poursuit avec la Direction générale des finances publiques afin de tirer parti de cette ouverture. | Poursuivre les travaux autour d'[OpenFisca](http://www.openfisca.fr/). | Partiel

## Une belle histoire : 
A l'occasion du hackathon #CodeImpot, le groupe projet "Optimisation du calcul des impôts"' a exploré une liste de techniques qui peuvent être mises en oeuvre pour obtenir une implémentation très rapide du calcul des impôts, de l'ordre de 10 000 situations fiscales calculées par seconde sur un ordinateur portable de moyenne gamme. Ces résultats laissent envisager des outils de simulation de réformes fiscales rapides à destination des économistes.
[En savoir plus (équipe, codes, rapport technique, etc.)](https://forum.openfisca.fr/t/optimisation-du-temps-de-calcul/78)

## Ils en parlent : 

- MesFinancesTV, [Reportage vidéo sur le hackathon #CodeImpôt](https://youtu.be/1yvcUcsRbkg), 4 avril 2016
- Fance Info, ["Un hackathon pour mieux comprendre l’impôt"](http://www.franceinfo.fr/actu/economie/article/un-hackathon-pour-mieux-comprendre-l-impot-779189), 1er avril 2016
- Rue89, ["Deux jours pour hacker les impôts"](http://rue89.nouvelobs.com/2016/04/01/deux-jours-hacker-les-impots-263649), 1er avril 2016
- Le Figaro, [Le gouvernement lance un hackathon autour des impôts"](http://www.lefigaro.fr/secteur/high-tech/2016/04/01/32001-20160401ARTFIG00248-le-gouvernement-lance-un-hackathon-autour-des-impots.php), 1er avril 2016
