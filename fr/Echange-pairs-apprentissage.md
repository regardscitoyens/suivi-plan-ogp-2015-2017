# 5. Echange entre pairs et apprentissage

Le [Partenariat pour un gouvernement ouvert](http://www.opengovpartnership.org/) est une plateforme de partage d'expérience pour la communauté des réformateurs de l'Etat à travers le monde. Ce réseau  permet aux pays membres d'échanger des bonnes pratiques et des ressources pour accompagner les démarches d'ouverture et de transparence.

La France porte plusieurs initiatives fortes d'échange entre pairs et de coopération internationale, y compris au niveau technique. L'ouverture et la réutilisation des méthodes et outils du gouvernement ouvert font partie des principes qui guident l'action de la France. Il s'agit en effet de construire et pérenniser des ressources communes du gouvernement ouvert.

## Partage et réutilisation de code de la plateforme data.gouv.fr à l'international

Depuis 2015, des collaborations techniques se créent avec différents pays autour de **udata**, le moteur de la plateforme [data.gouv.fr](https://www.data.gouv.fr/fr/), développé depuis 2013 par l'équipe d'Etalab [disponible de façon ouverte sur Github](https://github.com/opendatateam/udata). Etalab a donc créé une communauté de contributeurs à udata via [un service de discussion et d'entraide](https://gitter.im/etalab/udata).

**Collaborations techniques en cours** :
- **Avec le Luxembourg** : 
    * Depuis le début de l'année 2016, les équipes de [data.gouv.fr](https://www.data.gouv.fr/fr/) accompagnent les développeurs luxembourgeois dans la prise en main du code. En avril 2016, le Luxembourg a officialement lancé son portail d'ouverture des données, en version [anglaise](https://data.public.lu/en/) et [française](https://data.public.lu/fr/) lors d'un événement intitulé "[Game of Code](http://www.gameofcode.eu/)". L'objectif pour le Luxembourg est d'offrir un outil qui permettent de développer des services et crée de la valeur via les réutilisations.
    * Cette coopération a permis aux équipes de data.gouv.fr d'améliorer le code de [data.gouv.fr](https://www.data.gouv.fr/fr/) et [sa documentation](https://udata.readthedocs.io/en/latest/). Depuis, la refonte du parcours d'inscription au site de [data.gouv.fr](https://www.data.gouv.fr/fr/) a été entreprise.
    * **[En savoir plus](https://www.etalab.gouv.fr/lancement-de-la-plateforme-udata-du-luxembourg)**

![plateforme-lu](images/screenshot-plateforme-lu.PNG)

- **Avec la Serbie** :
    * Deux développeurs du gouvernement serbe travaillent sur l'adoption de udata pour le portail des données ouvertes de la Serbie.
- **Avec le Togo** : 
    * Le Togo a démarré son projet de création du portail opendata.tg en se basant sur udata. Une équipe de deux développeurs est en charge du projet et échange sur Git avec les développeurs français de [data.gouv.fr](https://www.data.gouv.fr/fr/) et les développeurs luxembourgeois de [data.public.lu](https://data.public.lu/fr/).  
    * Au-delà de l'adoption du portail, la mission Etalab a également échangé avec le Togo sur son expérience en matière de collecte des données auprès des administrations publiques. 

## Echanges et projets autour des données des pays francophones : #HackFrancophonie

Les 19 et 20 février 2016, près de dix pays francophones se sont réunis à Paris, pour un événement contributif organisé par le [Partenariat pour un gouvernement ouvert](http://www.opengovpartnership.org/), [Etalab](http://www.etalab.gouv.fr/), [Burkina Open Data Initiative](http://www.anptic.gov.bf/index.php/actualites/news/94-burkina-open-data-initiative-bodi), la [Banque Mondiale](http://donnees.banquemondiale.org/) et [CFI, l'agence française de coopération médias](http://donnees.banquemondiale.org/).
- L'objectif était d'accélérer les initiatives et projets de réutilisations de données pour le développement
- En amont de l'événement, les participants et les équipes d'Etalab ont recencé les données ouvertes dans un [répertoire ouvert](https://github.com/etalab/HackFrancophonie/wiki)
- Pendant ces deux jours, la réunion d'acteurs publics de l'open data, du développement et de nombreuses organisations de la société civile francophone ([jokkolabs](http://jokkolabs.net/), [Africtivistes](http://www.africtivistes.org/), [Social Justice](http://www.socialjustice-ci.net/), [Balai citoyen](https://fr-fr.facebook.com/CitoyenBalayeur)) a permis d'échanger des bonnes pratiques en matière d'open data (comment développer un portail, animer un écosystème de réutilisateurs, etc.). Deux correspondants open data ont présenté leurs missions et leurs réalisations au sein de leurs ministères. Une session sur le [Partenariat pour un gouvernement ouvert](http://www.opengovpartnership.org/) a également permis de présenter la démarche et ses réussites.
- Neuf projets concrets ont été développés sur des enjeux de cartographie, d'éducation, d'économie, et de démocratie

**Pays participants et jeux de données :** [Bénin](https://github.com/etalab/HackFrancophonie/wiki/B%C3%A9nin), [Burkina Faso](https://github.com/etalab/HackFrancophonie/wiki/Burkina-Faso), [Côte d’Ivoire](https://github.com/etalab/HackFrancophonie/wiki/C%C3%B4te-d%27Ivoire), [Haïti](https://github.com/etalab/HackFrancophonie/wiki/Ha%C3%AFti), [Ile Maurice](https://github.com/etalab/HackFrancophonie/wiki/Ile-Maurice), [Mali]((https://github.com/etalab/HackFrancophonie/wiki/Mali)) et [Sénégal](https://github.com/etalab/HackFrancophonie/wiki/S%C3%A9n%C3%A9gal).

![hackfrancophonie](images/hackfrancophonie.jpg) 

**Projets développés :** 

**Thème** | **Projet** 
--- | ---
Cartographie | [Cartographie des réseaux de bus](https://github.com/etalab/HackFrancophonie/wiki/Cartographie-des-r%C3%A9seaux-urbains-de-bus) 
Cartographie | [Co-construire une souveraineté cartographique](http://tasks.hotosm.org/project/1075)
Education | [Nos écoles, nos données V2](http://nendo.data.gov.bf/appli_accueil.php)
Education | [Ecoles au Mali : où investir ?](http://nbviewer.jupyter.org/format/slides/url/www.stats4dev.com/prez/Presentation_Hackfrancophonie.ipynb#/)
Démocratie | [Fais GAF : Gouvernance Accès Facile](https://github.com/AlexisEidelman/ConseilMinistreBurkina)
Démocratie | [Data Village](http://403debc4-bffb-4f28-bba4-56289f6bb953.pub.cloud.scaleway.com/)
Démocratie | [Parlement 2.0](https://github.com/etalab/HackFrancophonie/wiki/Democratie-2.0-au-S%C3%A9n%C3%A9gal)
Economie | [Filière coton](https://github.com/etalab/HackFrancophonie/wiki/Atelier-Fili%C3%A8re-Coton)
Economie | [Transparence financière Ebola](https://github.com/etalab/HackFrancophonie/wiki/Transparence-budget-%3A-crise-Ebola)

**En savoir plus :** 
- [Répertoire HackFrancophonie](https://github.com/etalab/HackFrancophonie/wiki)
- Résumé du jour 1 - [Ateliers sur l'ouverture des données entre gouvernements francophones](https://www.etalab.gouv.fr/hackfrancophonie-jour-1-ateliers-sur-louverture-des-donnees-entre-gouvernements-francophones)
- Résumé du jour 2 - [Open Data Camp autour des données ouvertes dans la Francophonie](https://www.etalab.gouv.fr/hackfrancophonie-jour-2-open-data-camp-autour-des-donnees-ouvertes-dans-la-francophonie)
- [Présentation générale de l'événement](https://www.etalab.gouv.fr/hackfrancophonie-un-open-data-camp-autour-des-donnees-ouvertes-par-les-pays-francophones)

## Accueil des délégations étrangères ou territoriales par la mission Etalab

La mission Etalab accueille régulièrement dans ses locaux des délégations étrangères ou territoriales de passage dans la capitale française : 
- En janvier 2016, Etalab a accueilli une **délégation de l'Instance centrale de prévention de la corruption (ICPC) du Maroc**, qui fait partie du comité de pilotage pour le gouvernement ouvert au Maroc. Les échanges ont notamment porté sur l'utilisation des outils numériques afin de mieux communiquer avec le public et engager les ONG, et sur le portail national d'intégrité développé par l'ICPC.
- En avril 2016, Etalab a accueilli une **délégation portugaise** pour échanger autour de l'ouverture des données publiques, du gouvernement ouvert et des méthodes de datascience pour piloter les politiques publiques par la donnée.
- Début juin 2016, Etalab reçoit des **représentants de Nouvelle-Calédonie** intéressés par l'ouverture des données publiques. Cette rencontre témoigne de l'intérêt grandissant des acteurs territoriaux pour l'open data et l'open gov.
- Fin juin 2016, Etalab rencontre une **délégation indienne** suite à l'accord entre le Ministère du personnel, des doléances publiques et des retraites de l'Inde et le Ministère de la Fonction publique de la France concernant la réforme de l'administration.

## Formation de la nouvelle génération de l'open data : l'Open Data Youth Camp en Croatie

Du 29 août au 2 septembre 2015, 21 jeunes venus de Croatie, de Serbie, de France et du Royaume-Uni ont été initié aux principes et aux enjeux de l'ouverture des données à l'occasion d'un [Open Data Youth Camp](http://odbc.codeforcroatia.org/) à Rovinj, en Croatie. Grâce au [gouvernement de Croatie](https://udruge.gov.hr/en), à l'[ambassade de France en Croatie](http://www.ambafrance-hr.org/) et à la [mission Etalab](http://etalab.gouv.fr/), [trois jeunes participants et une experte](https://www.etalab.gouv.fr/les-jeunes-et-lopen-data) ont été sélectionnés pour représenter la France lors de cet événément. Après plusieurs jours de formations, témoigagnes et ateliers pratiques, les participants ont rédigé une Youth Open Data Declaration qui a été présentée à l'occasion du Sommet mondial 2015 du Partenariat pour un gouvernement ouvert à Mexico. 


![YouthODC](images/YouthODC.png)


[En savoir plus](https://www.etalab.gouv.fr/la-nouvelle-generation-de-lopen-data-retours-dexperience-de-lopen-data-youth-camp) 

## Coopération avec le Royaume-Uni sur l'économie de la donnée

- Le 27 juillet, George Osborne, le Chancellier de l'Echiquier du Royaume-Uni, et Emmanuel Macron, ministre de l'Economie, de l'Industrie et du Numérique de la France, ont annoncé la mise en place d'une "taskforce" franco-britannique sur l'économie de la donnée. Le Royaume-Uni et la France ont ainsi décidé d'unir leurs efforts pour comprendre comment exploiter au mieux le potentiel de croissance des données. Ce groupe de travail est présidé par Sir Nigel Shadbolt, président et co-fondateur de l’Open Data Institute, et Henri Verdier, administrateur général des données et directeur interministériel du numérique et du système d’information et de communication (DINSIC), au sein du secrétariat général pour la modernisation de l’action publique (SGMAP). Il vise les objectifs suivants : 
    * renforcer les collaborations entre entités publiques françaises et britanniques sur l’ouverture des données publiques et sur l’amélioration des services publics grâce aux données ;
    * mieux accompagner les entreprises (notamment les PME) dans l’exploitation des données publiques et privés, afin de renforcer leur compétitivité ;
    * promouvoir la culture des données et des compétences en sciences des données ; dans le secteur public comme dans le secteur privé ;
    * créer des conditions favorables au développement d’un écosystème de l’économie de la donnée, en renforçant notamment la confiance des utilisateurs et des entreprises.
- Après plusieurs réunions de travail, un rapport final identifiera des opportunités de collaboration concrète entre les deux pays  visant l’exploitation de la révolution des données par les entreprises, notamment par les startups et les PME, ainsi que la modernisation de l’Etat à travers le développement de services publics fondés sur la donnée.

En savoir plus [en français](https://www.etalab.gouv.fr/lavancement-de-la-taskforce-franco-britannique-sur-leconomie-de-la-donnee) ou [en anglais](https://www.gov.uk/government/news/new-boost-for-digital-economy)

## Participation au Partenariat mondial pour les données de développement durable

- Le 25 septembre 2015, les Etats membres de l'ONU ont adopté 17 [objectifs de développement durable](http://www.undp.org/content/undp/fr/home/mdgoverview/post-2015-development-agenda.html) (ODD) d'ici à 2030. Afin de permettre et de mesurer les progrès du développement, l'usage des données et la diffusion des principes du gouvernement ouvert sont essentiels. C'est pour approfondir ces thèmes qu'une réunion de niveau ministériel du comité directeur du [Partenariat pour un gouvernement ouvert](https://www.etalab.gouv.fr/ogp) et que le lancement du [Partenariat mondial pour les données du développement](http://www.data4sdgs.org/) ont été organisés en marge de l'Assemblée générale des Nations Unies. Annick Girardin, alors secrétaire d'Etat en charge du Développement et de la Francophonie, a porté la voix de la France dans ces deux événements. Elle a souligné dans son [discours](https://www.etalab.gouv.fr/wp-content/uploads/2015/10/Lundi-28-septembre-partenariat-mondiale-donn%C3%A9es-de-d%C3%A9veloppement.pdf) deux engagements de la France : promouvoir le sujet des données du développement au cours de la futur présidence française du PGO, et renforcer le soutien financier à l’Observatoire économique et statistique d’Afrique subsaharienne (Afristat). [En savoir plus](https://www.etalab.gouv.fr/mettre-les-donnees-au-service-des-objectifs-du-developpement-durable) 
- Dans la continuité de ces engagements, Annick Girardin et Axelle Lemaire, secrétaire d'Etat en charge du Numérique,ont présenté le 15 décembre leur [plan d'action gouvernemental "Développement et numérique"](http://www.diplomatie.gouv.fr/fr/politique-etrangere-de-la-france/diplomatie-numerique/evenements/article/developpement-numerique-presentation-par-annick-girardin-et-axelle-lemaire-du). Avec ce plan, la France est déterminée à accompagner les pays en développement dans leurs efforts en matière de numérique, qui contribueront au succès du nouvel agenda du développement durable adopté en septembre 2015.     

## Participation aux groupes de travail de l'OCDE

La mission Etalab participe aux groupes de travail organisés par l'OCDE afin de conseiller les pays membres du PGO dans l'élaboration de leur Plan d'action national, ainsi qu'à des rencontres internationales dans lesquelles elle porte les principes du gouvernement ouvert. 

## Participation aux groupes de travail du Partenariat pour un gouvernement ouvert

La France participe aux différents groupes de travail du Partenariat pour un gouvernement ouvert.  
Dans le cadre du groupe de travail "open data" du Partenariat pour un gouvernement ouvert, Etalab a :
- Sondé les 200 participants sur l'appréhension du concept de "données d'intérêt général" dans leurs pays
- Relu et commenté les engagements "open data" des Plans d'action nationaux de la Côte d'Ivoire, du Paraguay, de la Roumanie ou encore du Canada 

[En savoir plus](http://www.modernisation.gouv.fr/laction-publique-se-transforme/en-ouvrant-les-donnees-publiques/partenariat-pour-un-gouvernement-ouvert-espace-de-cooperation-internationale) 



