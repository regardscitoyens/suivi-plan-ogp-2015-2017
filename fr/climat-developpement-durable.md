# Le gouvernement ouvert au service du climat et du développement durable

- [Engagement 24 : Associer la société civile à la Conférence COP21 et favoriser la transparence sur l'agenda et les négociations](Engagement24.md)
- [Engagement 25 : Mettre à disposition des données et des modèles relatifs au climat et au développement durable](Engagement25.md)
- [Engagement 26 : Engager de nouvelles collaborations avec la société civile afin de développer des solutions innovantes pour répondre aux défis du climat et du développement durable](Engagement26.md)