# Rendre des comptes

- [Engagement 1 : Permettre à tous de consulter, de comprendre et de réutiliser les données financières et les décisions des collectivités territoriales](Engagement1.md)
- [Engagement 2 : Accroître la transparence de la commande publique](Engagement2.md)
- [Engagement 3 : Accroître la transparence de l'aide publique au développement](Engagement3.md)
- [Engagement 4 : Ouvrir l'accès aux évaluations de politiques publiques et à leurs conclusions](Engagement4.md)
- [Engagement 5 : Impliquer davantage les citoyens dans les travaux menés par la Cour des comptes](Engagement5.md)
- [Engagement 6 : Faciliter l'accès aux données relatives aux obligations de transparence des responsables publics](Engagement6.md)
- [Engagement 7 : Identifier les bénéficiaires effectifs des entités juridiques enregistrées en France pour lutter efficacement contre le blanchiment](Engagement7.md)
- [Engagement 8 : Renforcer la transparence des paiements et revenus issus des industries extractives](Engagement8.md)
- [Engagement 9 : Accroître la transparence sur les négociations commerciales internationales](Engagement9.md)