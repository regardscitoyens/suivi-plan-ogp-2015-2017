# Engagement 26 : Engager de nouvelles collaborations avec la société civile afin de développer des solutions innovantes pour répondre aux défis du climat et du développement durable

## Institutions porteuses : 
- Ministère de l'Environnement, de l'Energie et de la Mer
- Secrétariat d'Etat chargé de la Réforme de l'Etat et de la Simplification
- Météo France
- Institut national de l'information géographique et forestière
- Centre national d'études spatiales

## Enjeux : 
En complément de l’accord international attendu à Paris, des initiatives concrètes associant gouvernements et acteurs non-étatiques peuvent être développées. C’est le projet de l’ « agenda des solutions »39, qui vise à soutenir et amplifier les engagements des États dans la réduction des émissions de gaz à effet de serre, dans l’adaptation aux impacts du dérèglement climatique et dans le financement de ces actions.

## Description de l'engagement : 
- **Lancer et organiser les premières étapes de l’opération Climate Change Challenge (C3) durant toute l’année 2015**
- **Récompenser les lauréats de l'opération C3 à l’occasion de la Conférence COP21**
    - Des lauréats du Challenge Climat organisé similairement par le Mexique seront également présents
- **Poursuivre l’opération en 2016 et 2017**
    - Suivre et accompagner les meilleurs projets d’innovation, en expertise et incubation, capitaliser sur les meilleurs défis pour émettre de nouveaux appels à projets, pérenniser les outils en ligne d’expression citoyenne

[En savoir plus](http://gouvernement-ouvert.etalab.gouv.fr/content/fr/climat/engagement-26.html)
 
## Description des résultats :

**Actions** | **Résultats** | **Prochaines étapes** | **Statut**
--- | --- | --- | ---
Lancer et organiser les premières étapes de l’opération C3 durant toute l’année 2015. | Les trois étapes du [C3](http://c3challenge.com/) ont été menées dans quatre villes de France (Paris, Lyon, Toulouse et Nantes) au cours de l'année 2015. Lors de la dernière étape, 29 projets portés par 150 participants ont été présentés devant le jury. | | Complet
Récompenser les lauréats de l'opération C3 à l'occasion de la COP21 en présence des lauréats mexicains. | Les six lauréats nationaux sélectionnés ont été primés le 5 décembre à l'occasion d'un "side event" dans la zone société civile de la COP21, en présence des trois lauréats mexicains. Les projets des lauréats français et mexicains ont également été présentés lors de trois événements organisés par l'Agence française du développement, Solutions 21 et l'équipe open data du Mexique. La démarche du C3 était par ailleurs présentée sur le stand "Numérique et climat" du pavillon de la France dans la zone des négociations de la COP21. | | Complet
Poursuivre l'opération en 2016 et 2017 : suivre et accompagner les meilleurs projets d’innovation, en expertise et incubation, capitaliser sur les meilleurs défis pour émettre de nouveaux appels à projets, pérenniser les outils en ligne d’expression citoyenne | Les lauréats nationaux et régionaux ont gagné un accompagnement de pré-incubation et les partenaires de l'opération C3 ont identifié les projets qu'ils allaient continuer à suivre. En 2016, le ministère de l'Environnement, de l'Energie et de la Mer a capitalisé sur les défis pour émettre deux nouveaux appels à projets sur la consommation énergétique et la biodiversité. La plateforme ["100 projets pour le climat"](http://100projetspourleclimat.gouv.fr/fr/) a été ouverte à contribution et vote : d'ici le 6 juillet 2016, 100 projets proposant des solutions pour lutter contre le réchauffement climatique seront sélectionnés et seront valorisés lors de la COP22 à Marrakech. | Continuer à suivre les projets lauréats et leur éventuelle incubation par les partenaires du C3. | Substantiel

## Une belle histoire : 

Le succès de l'opération C3 a inspiré le ministère de l'Environnement, de l'Energie et de la Mer qui a lancé sa propre démarche [Green Tech](http://www.developpement-durable.gouv.fr/GreenTech-les-technologies-vertes.html) pour promouvoir l'innovation au service du développement durable en collaboration avec la société civile. Dans ce cadre, le ministèrea annoncé l'ouverture d'un incubateur "Green Tech Verte" dédié aux technologies vertes qui sera installé en septembre 2016. Le ministère a organisé deux premiers hackathons sur la [consommation énergétique](http://www.developpement-durable.gouv.fr/Hackathon-CompteurConnect-le.html) (du 20 au 22 mai 2016) et les [données de la biodiversité](http://www.developpement-durable.gouv.fr/Venez-participer-au-premier.html) (du 3 au 5 juin 2016). Les lauréats de ces hackathons, tout comme ceux du concours Green Tech Verte école seront intégrés à cet incubateur.

## Ils en parlent : 
- La Tribune Toulouse, ["COP21 : quand open data et intelligence collective se mettent au service du climat"](http://objectifnews.latribune.fr/evenements/forum-climat-cop21/2015-10-06/cop21-quand-open-data-et-intelligence-collective-se-mettent-au-service-du-climat.html), 6 octobre 2015
- La Gazette des Communes, ["OpenSolarMap, le soleil en partage"](http://www.lagazettedescommunes.com/425060/open-solar-map-le-soleil-en-partage/), 12 janvier 2016
- Le Monde, ["Le gouvernement donne un coup de main aux start-ups de la transition énergétique"](http://www.lemonde.fr/planete/article/2016/02/09/le-gouvernement-donne-un-coup-de-main-aux-start-ups-de-la-transition-energetique_4862289_3244.html), 9 février 2016
- Le Moniteur, ["Un hackathon pour innover dans les économies d'énergie"](http://www.lemoniteur.fr/article/un-hackathon-pour-innover-dans-les-economies-d-energie-32166748), 27 avril 2016


