# Engagement 17 : Transformer les ressources technologiques de l'État en plateforme ouverte

## Institutions porteuses : 
- Premier ministre
- Secrétariat d'État chargé de la Réforme de l'État et de la Simplification

## Enjeux : 
Les stratégies technologiques des géants du numérique démontrent chaque jour la puissance, pour une organisation, d'une approche accordant une place centrale à l'ouverture, l'interopérabilité et l'agilité des systèmes, entièrement orientée vers l'expérience utilisateur et la satisfaction des besoins des usagers, qui n'exclue en rien la puissance ou la sécurité.
Inspirée des principes de « Government as a platform », la stratégie « État plateforme et France Connect » propose une transformation technologique majeure des principes de l'informatique de l'État afin de faciliter l'accès aux données, l'interopérabilité des systèmes et la réutilisation des développements réalisés par la sphère publique.
Ces principes offrent de nouvelles perspectives, car ils débrident l'innovation dans la conception de nouveaux services aussi bien pour les particuliers que pour les entreprises.

## Description de l'engagement : 
- **Valider le cadre stratégique « État Plateforme et France Connect » et en décliner les grands principes au cours de l'année 2015 dans les référentiels généraux ou documents normatifs émis par la Direction interministérielle des systèmes d'information et de communication**
- **Lancer France Connect sur le portail service-public.fr**
    - Aboutissement du projet France Connect concrétisé par le démarrage des premières expérimentations à l'automne 2015, suivi d'un lancement en janvier 2016 sur le portail service-public.fr (plusieurs millions d'utilisateurs)
    - La généralisation se déroulera à partir de 2016
- **Mettre en service la « Forge » publique sur le site etatplateforme.modernisation.gouv.fr et d'un magasin d'API sur le site d'ici fin 2015 pour inciter à la création de nouveaux services de manière collaborative**
- **Lancer plusieurs cycles de sensibilisation pour le développement d'API et la création de nouveaux services auprès des acteurs de la sphère publique et de ses partenaires**

[En savoir plus](http://gouvernement-ouvert.etalab.gouv.fr/content/fr/partager-des-ressources/engagement-17.html)
 
## Description des résultats :

**Actions** | **Résultats** | **Prochaines étapes** | **Statut**
--- | --- | --- | ---
Valider le cadre stratégique « État Plateforme et France Connect » et en décliner les grands principes au cours de l'année 2015 dans les référentiels généraux ou documents normatifs émis par la Direction interministérielle des systèmes d'information et de communication. | La version 2.0 du référentiel général d'interopérabilité a été officialisée par l'[arrêté du 20 avril 2016](https://www.legifrance.gouv.fr/eli/arrete/2016/4/20/PRMJ1526716A/jo). Pour simplifier le travail des collectivités intéressées, une [version finalisée de la documentation nécessaire](http://modernisation.gouv.fr/ladministration-change-avec-le-numerique/par-son-systeme-dinformation/le-referentiel-general-interoperabilite-fait-peau-neuve) a été mise en ligne le 27 avril 2016. | | Complet
Lancer France Connect sur le portail [service-public.fr](https://www.service-public.fr/). | Les expérimentations ont démarré en septembre 2015. La phase de généralisation a démarré le 1er avril 2016 et se prolongera pendant plusieurs mois. Le nouveau portail [service-public.fr](https://www.service-public.fr/) a notamment ouvert en mars 2016 avec France Connect. Déjà, plusieurs dizaines services publics numériques de l’Etat sont en train d’intégrer France Connect. | Le site dédié à France Connect est ouvert et sera accessible avec l’adresse [franceconnect.gouv.fr](https://franceconnect.gouv.fr/). Un lancement public est en projet d'ici l'été 2016. Le magasin d’API rebaptisé [api.gouv.fr](https://api.beta.gouv.fr/) ouvrira dans les prochains mois.  | Substantiel
Mettre en service la « Forge » publique sur le site [etatplateforme.modernisation.gouv.fr](http://etatplateforme.modernisation.gouv.fr/) et d'un magasin d'API sur le site d'ici fin 2015 pour inciter à la création de nouveaux services de manière collaborative. | Le site [etatplateforme.modernisation.gouv.fr](http://etatplateforme.modernisation.gouv.fr/) est ouvert. Le site [api.gouv.fr](https://api.beta.gouv.fr/) devrait être lancé dans les prochains mois. La forge n'apparaît plus comme nécessaire pour faire de l'APIfication. | | Substantiel
Lancer plusieurs cycles de sensibilisation pour le développement d'API et la création de nouveaux services auprès des acteurs de la sphère publique et de ses partenaires. | Des séances d’information ou de sensibilisation sont organisées mensuellement. Un dispositif spécial d’accompagnement (baptisé « ATENA ») à la conception de nouveaux services publics numériques a été mis en place. 70 projets sont déjà identifiés dont une vingtaine sont soutenus par le [Programme d'investissements d'avenir](http://www.gouvernement.fr/investissements-d-avenir-cgi) (PIA) au titre des appels à projet « Identité numérique & relation usager » et « Dites-le nous une fois particulier ». | | Substantiel

## Ils en parlent : 
IT Espresso, ["Service-public.fr fait la jonction avec France Connect"](http://www.itespresso.fr/service-public-fr-jonction-france-connect-123661.html), 11 mars 2016