# Engagement 15 : Renforcer la politique d'ouverture et de circulation des données

## Institutions porteuses : 
- Secrétariat d'État chargé de la Réforme de l'État et de la Simplification
- Secrétariat d'État chargé du Numérique

## Enjeux : 
La France, son gouvernement et ses collectivités territoriales se sont engagés avec force dans l'ouverture et le partage des données publiques. Cette politique essentielle est considérée à la fois comme un ressort de vitalité démocratique, une stratégie d'aide à l'innovation économique et sociale, et un levier pour la modernisation de l'action publique.

## Description de l'engagement : 
- **Poursuivre l'ouverture des données à fort impact économique et social, et notamment des « données-pivot »**
- **Renforcer l'open data des collectivités territoriales : inscrire dans la loi l'obligation de publier les informations publiques des collectivités de plus de 3500 habitants (y compris communes et EPCI)**
- **Inscrire dans la loi les principes d'ouverture par défaut des données publiques (avec fermeture par exception) et de leur réutilisation libre et gratuite**
- **Approfondir l'étude d'opportunité sur l'ouverture des « données d'intérêt général »** 

[En savoir plus](http://gouvernement-ouvert.etalab.gouv.fr/content/fr/partager-des-ressources/engagement-15.html)
 
## Description des résultats :

**Actions** | **Résultats** | **Prochaines étapes** | **Statut**
--- | --- | --- | ---
Poursuivre l'ouverture des données à fort impact économique et social, et notamment des « données-pivot ». | L'article 4 du [projet de loi pour une République numérique](https://www.legifrance.gouv.fr/affichLoiPreparation.do?idDocument=JORFDOLE000031589829&type=general&typeLoi=proj&legislature=14) instaure la notion de données à intérêt économique et social. Dans ce cadre, [l'ouverture de la base SIRENE est prévue au 1er janvier 2017](https://www.etalab.gouv.fr/louverture-du-repertoire-sirene-par-linsee-au-1er-janvier-2017-une-avancee-majeure-pour-lopen-data). Des travaux sont en cours afin de définir les données pivots (jeux de données de qualité qui serviront de référence. En parallèle, un travail a été fait sur la montée en qualité des données disponibles sur la plateforme data.gouv.fr (mise en place d'un indicateur de qualité pour les producteurs de données et relance automatique lorsqu'une mise à jour des données est nécessaire). | Définir les données pivots et appliquer les dispositions contenues dans le projet de loi numérique | Partiel
Renforcer l'open data des collectivités territoriales : inscrire dans la loi l'obligation de publier les informations publiques des collectivités de plus de 3500 habitants (y compris communes et EPCI). | Cet engagement a été inscrit dans la [loi du 7 août 2015 portant nouvelle organisation territoriale de la République](https://www.legifrance.gouv.fr/affichLoiPubliee.do;jsessionid=A4F27320020462891B15569F43C59812.tpdila12v_1?idDocument=JORFDOLE000029101338&type=contenu&id=2&typeLoi=&legislature=14) (article 30). | Suivre les décrets d'application de la loi et accompagner les collectivités de plus de 3 500 habitants dans l'ouverture effective de leurs données. | Partiel 
Inscrire dans la loi les principes d'ouverture par défaut des données publiques (avec fermeture par exception) et de leur réutilisation libre et gratuite. | Le [projet de loi pour une République numérique](https://www.legifrance.gouv.fr/affichLoiPreparation.do?idDocument=JORFDOLE000031589829&type=general&typeLoi=proj&legislature=14) pose le principe d'une ouverture par défaut des données publiques. La [loi du 28 décembre 2015 relative à la gratuité et aux modalités de la réutilisation des informations du secteur public](https://www.legifrance.gouv.fr/affichTexte.do;jsessionid=7E3851E32FA0CB87DD653ABFF4A797F0.tpdila20v_3?cidTexte=JORFTEXT000031701525&categorieLien=id) pose le principe de la gratuité de la réutilisation des informations publiques. | Suivre l'examen du projet de loi pour une République numérique en Commission mixte paritaire et prendre les décrets qui définissent les administrations n'ayant pas d'obligation de gratuité | Partiel
Approfondir l'étude d'opportunité sur l'ouverture des « données d'intérêt général ». | Jugée opportune, l'ouverture des données d'intérêt général a été inscrite dans le [projet de loi pour une République numérique](https://www.legifrance.gouv.fr/affichLoiPreparation.do?idDocument=JORFDOLE000031589829&type=general&typeLoi=proj&legislature=14) pour les contrats de concessions et les subventions. Une nouvelle mission est en cours sur les données d'intérêt général. | Suivre l'examen du projet de loi pour une République numérique en Commission mixte paritaire et les conclusions de la mission en cours  et rendre applicables ces dispositions | Partiel

## Une belle histoire : 

Suite à une mobilisation des associations de lycéens, le ministère de l'éducation a rendu publiques les règles s'appliquant à la répartition des élèves dans le secondaire. Malgré cette bonne nouvelle, les requérants souhaitent que le code source du logiciel gérant ces affectations soit rendu public ce que pour l'instant le ministère refuse.
