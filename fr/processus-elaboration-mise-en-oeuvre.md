# 2. Processus d'élaboration et de mise en oeuvre du plan d'action national

Cette partie présente les mécanismes de consultation mis en place lors de l'élaboration du Plan d'action national, de sa mise en oeuvre et de l'élaboration du rapport d'autoévaluation.

- [2.1. Consultation lors de l'élaboration du Plan d'action national](consultation-elaboration.md )
- [2.2. Consulation lors de la mise en oeuvre du Plan d'action national](consultation-mise-en-oeuvre.md)
- [2.3. Consultation sur le rapport d'autoévaluation](consultation-rapportautoeval.md )