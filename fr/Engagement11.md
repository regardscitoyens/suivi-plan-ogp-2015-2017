# Engagement 11 : Coproduire avec la société civile les registres-clés de données essentielles à la société et à l'économie

## Institutions porteuses
- Secrétariat d'État chargé de la Réforme de l'État et de la Simplification
- Secrétariat d'État chargé du Numérique

## Enjeux : 
**De nouvelles formes de coopérations entre les autorités publiques et les citoyens permettent désormais de créer de nouveaux biens communs**, indispensables au service public, à la société et à l'économie, d'une manière plus rapide, plus efficace et moins coûteuse que par le passé.

## Description de l'engagement : 

**Multiplier les coopérations entre acteurs publics et société civile pour la constitution d'infrastructures de données essentielles et de registres-clés de données.**

[En savoir plus](http://gouvernement-ouvert.etalab.gouv.fr/content/fr/consulter-concerter-co-produire/action-publique/engagement-11.html)
 
## Description des résultats :

**Actions** | **Résultats** | **Prochaines étapes** | **Statut**
--- | --- | --- | ---
Multiplier les coopérations entre acteurs publics et société civile pour la constitution d’infrastructures de données essentielles et de registres-clé de données. | Le [projet de loi pour une République numérique]( https://www.legifrance.gouv.fr/affichLoiPreparation.do?idDocument=JORFDOLE000031589829&type=general&typeLoi=proj&legislature=14) donnera des obligations législatives sur le service public de la donnée. Le travail sur le décret a commencé. Des coopérations sont prévues autour de données essentielles : la [base adresse nationale](https://www.data.gouv.fr/fr/datasets/ban-base-adresse-nationale/) (BAN) est en cours, la base des établissements recevant du public (ERP) est en projet à l'IGN et au SGMAP, des actions sont initiées pour l'ouverture du registre national des associations (RNA), pressenti pour figurer parmi les bases de données de références du « service public de la donnée » (introduit par le projet de loi pour une République numérique), le registre des électeurs fait l’objet d’un chantier interministériel associant l'INSEE et le ministère de l’Intérieur. | Mener un travail de concertation avant la fin de l'été pour décider quels sont les registres clés qui seront inclus dans le décret. | Partiel
