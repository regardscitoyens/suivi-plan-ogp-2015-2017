# Engagement 21 : Diffuser la culture de l'ouverture, des données et du numérique


## Institutions porteuses : 
- Ministère de la Fonction publique ;
- Secrétariat d'État chargé de la Réforme de l'État et de la Simplification auprès du Premier ministre ;
- École nationale d'administration

## Enjeux : 
Les agents publics doivent être les acteurs des changements permis par la révolution numérique, la politique d'ouverture et de partage des données publiques et de gouvernement ouvert.

## Description de l'engagement : 
- **Produire, en co-construction avec la société civile, des modules de formation à l'ouverture et à l'utilisation des données et au gouvernement ouvert à destination des agents publics**
- **Inclure dans la formation initiale et la formation continue des écoles de formation des agents publics nationaux et territoriaux davantage de modules sur l'ouverture et l'utilisation des données et sur le gouvernement ouvert**
    - Intégrer ces modules dans les cursus de l'ENA, de l'École de la Modernisation de l'État (pour la formation continue) et de toute autre école qui souhaiterait relayer ces enjeux
- **Mettre en place un dispositif de sensibilisation des directeurs d'administration centrale aux enjeux du numérique, et d'accompagnement à la mise en œuvre de projets de transformation numérique**
    - Identifier les besoins, difficultés, souhaits des directeurs d'administrations centrales sur les enjeux de transformation numérique de la société et des politiques publiques dans leurs champs d'actions
    - Sensibiliser les directeurs d'administration centrale à l'automne, lors d'un séminaire sur les enjeux et solutions offertes par le numérique pour répondre à leurs préoccupations
    - Inclure un volet sur les enjeux et solutions offerts par le numérique lors d'un séminaire des directeurs d'administration centrale, à l'automne
    - Prévoir ultérieurement de diffuser plus largement ces modules


[En savoir plus](http://gouvernement-ouvert.etalab.gouv.fr/content/fr/ouvrir-l-administration/ouverture-et-innovation/engagement-21.html)
 
## Description des résultats :

**Actions** | **Résultats** | **Prochaines étapes** | **Statut**
--- | --- | --- | ---
Produire, en co-construction avec la société civile, des modules de formation à l'ouverture et à l'utilisation des données et au gouvernement ouvert à destination des agents publics | En mars 2016, le CNAM Paris et le CNAM Bretagne ont lancé le [certificat de spécialisation "Innovations territoriales et données numériques"](http://formation.cnam.fr/par-ecole/ecole-ms/villes-echanges-territoires/certificat-de-specialisation-innovations-territoriales-et-donnees-numeriques-777352.kjsp). Ils s'adressent aux professionnels (agents des territoires, etc.) et aux personnes souhaitant compléter leur formation. | Travail en cours sur la "littératie" sur l'open data, avec une chercheuse intégrée chez Etalab. Faire remonter d'autres initiatives de formation développées en coconstruction avec la société civile. | Partiel
Inclure dans la formation initiale et la formation continue des écoles de formation des agents publics nationaux et territoriaux davantage de modules sur l'ouverture et l'utilisation des données et sur le gouvernement ouvert |  Des modules sur l'open data sont en cours avec l'Ecole de la Modernisation de l'Etat. Le SGMAP travaille sur un catalogue de formation à l'open data, notamment des intégrés dans la FAQ de data.gouv.fr. Lancelot Pecquet, représentant de la société civile, donne des [formations open data à l'ENA](http://www.ena.fr/content/download/4768/73442/version/1/file/Programme Partage des donn%C3%A9es.pdf). | Développer le catalogue de formation et définir un délai pour la publication des modules de formation dans la FAQ de data.gouv.fr. Le rapport de la taskforce UK/FR va peut-être préconiser de traduire et utiliser les solutions qui existent dans d'autres pays et qui fonctionnent (par exemple les serious games de l'ODI pour apprendre à utiliser les données). Travailler avec l'ENA, l'EME, Sciences Po, etc. pour approfondir. | Partiel
Mettre en place un dispositif de sensibilisation des directeurs d'administration centrale aux enjeux du numérique, et d'accompagnement à la mise en œuvre de projets de transformation numérique | Un séminaire a été organisé par le Secrétariat général du gouvernement à destination des Directeurs d'administration centrale (DAC). | Suivre l'action du ministère de l'Intérieur et du Commissariat général au développement durable qui souhaitent organiser des formations sur le numérique. | Partiel

