# 4.2. Détail des engagements et résultats obtenus

# Rendre des comptes

**Renforcer la transparence de la dépense et des comptes publics** 

- [Engagement 1 : Permettre à tous de consulter, de comprendre et de réutiliser les données financières et les décisions des collectivités territoriales](Engagement1.md)
- [Engagement 2 : Accroître la transparence de la commande publique](Engagement2.md)
- [Engagement 3 : Accroître la transparence de l'aide publique au développement](Engagement3.md)

**Ouvrir l'évaluation publique**

- [Engagement 4 : Ouvrir l'accès aux évaluations de politiques publiques et à leurs conclusions](Engagement4.md)
- [Engagement 5 : Impliquer davantage les citoyens dans les travaux menés par la Cour des comptes](Engagement5.md)

**Renforcer la transparence sur les déclarations d'intérêts et de patrimoine des responsables publics**

- [Engagement 6 : Faciliter l'accès aux données relatives aux obligations de transparence des responsables publics](Engagement6.md)

**Favoriser la transparence de la vie économique**

- [Engagement 7 : Identifier les bénéficiaires effectifs des entités juridiques enregistrées en France pour lutter efficacement contre le blanchiment](Engagement7.md)
- [Engagement 8 : Renforcer la transparence des paiements et revenus issus des industries extractives](Engagement8.md)
- [Engagement 9 : Accroître la transparence sur les négociations commerciales internationales](Engagement9.md)

# Consulter, concerter, co-produire

**Favoriser une action publique contributive et collaborative**

- [Engagement 10 : Donner aux citoyens de nouveaux moyens de participer à la vie publique en les associant à l'identification de problèmes à résoudre](Engagement10.md)
- [Engagement 11 : Coproduire avec la société civile les registres-clés de données essentielles à la société et à l'économie](Engagement11.md)
- [Engagement 12 : Poursuivre l'ouverture des ressources juridiques et la collaboration avec la société civile autour de l'élaboration de la loi](Engagement12.md)

**Rénover les pratiques de consultation et de concertation**

- [Engagement 13 : Capitaliser sur les consultations menées et rénover les dispositifs d'expression citoyenne](Engagement13.md)
- [Engagement 14 : Renforcer la médiation et la capacité d'agir des citoyens en matière de justice](Engagement14.md)

# Partager des ressources numériques

- [Engagement 15 : Renforcer la politique d'ouverture et de circulation des données](Engagement15.md)
- [Engagement 16 : Favoriser l'ouverture des modèles de calcul et des simulateurs de l'État](Engagement16.md)
- [Engagement 17 : Transformer les ressources technologiques de l'État en plateforme ouverte](Engagement17.md)
- [Engagement 18 : Mieux interagir avec l'usager et améliorer les services publics grâce à l'administration numérique](Engagement18.md)

# Poursuivre l'ouverture de l'administration

**Favoriser l'engagement citoyen en soutien des pouvoirs publics**

- [Engagement 19 : Permettre l'engagement de la société civile en appui de l'École](Engagement19.md)

**Ouvrir l'accès à la fonction publique**

- [Engagement 20 : Diversifier le recrutement au sein des institutions publiques](Engagement20.md)

**Diffuser la culture de l'ouverture et l'innovation dans l'administration**

- [Engagement 21 : Diffuser la culture de l'ouverture, des données et du numérique](Engagement21.md)
- [Engagement 22 : Diffuser l'innovation et approfondir la recherche sur le gouvernement ouvert](Engagement22.md)

**Renforcer la déontologie dans la fonction publique**

- [Engagement 23 : Responsabiliser et protéger les agents publics en matière de prévention des conflits d'intérêts](Engagement23.md)

# Le gouvernement ouvert au service du climat et du développement durable

- [Engagement 24 : Associer la société civile à la Conférence COP21 et favoriser la transparence sur l'agenda et les négociations](Engagement24.md)
- [Engagement 25 : Mettre à disposition des données et des modèles relatifs au climat et au développement durable](Engagement25.md)
- [Engagement 26 : Engager de nouvelles collaborations avec la société civile afin de développer des solutions innovantes pour répondre aux défis du climat et du développement durable](Engagement26.md)

