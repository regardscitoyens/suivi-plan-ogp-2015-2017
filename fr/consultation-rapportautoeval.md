# 2.3. Consultation sur le rapport d'autoévaluation

Dans cette section seront intégrés ultérieurement des éléments sur les différentes périodes de consultation proposées autour du rapport d'autoévaluation : 
- Consultation de juin 2016 sur la première version du rapport d'autoévaluation à mi-parcours
- Processus de consultation en continu sur le rapport d'autoévaluation final (échéance : juillet 2017) et le prochain Plan d'action national de la France
- Contributions sur l'amélioration de la méthodologie de consultation et d'élaboration du plan d'action

