# Engagement 14 : Renforcer la médiation et la capacité d'agir des citoyens en matière de justice

## Institutions porteuses : 
- Ministère de la Justice

## Enjeux : 
C'est l'une des grandes missions de la justice que d'apaiser les relations sociales. Une justice plus efficace, des possibilités de recours multiples et des règlements de litige à l'amiable, contribuent au gouvernement ouvert. La justice doit aujourd'hui s'ouvrir à la société afin d'expliquer son fonctionnement, ses contraintes, et ses priorités et intégrer les besoins et retours du citoyen pour s'améliorer.

## Description de l'engagement : 
- **Ouvrir la justice à la société par la création de conseils de juridiction**
    - Créer des conseils de juridiction auprès des tribunaux de grande instance et des cours d'appel pour permettre une réflexion commune sur des problématiques transversales telles que l'aide juridictionnelle, l'accès au droit, l'accès à la justice, la conciliation, la médiation et l'aide aux victimes. Présidés par les chefs de juridiction, ces conseils de juridiction réuniront des magistrats du siège et du parquet, des fonctionnaires de la juridiction et de l'administration pénitentiaire et de la protection judiciaire de la jeunesse, des élus locaux, des organisations syndicales, des représentants locaux de l'État, des professions du droit, des collectivités locales, et des représentants associatifs.
- **Faciliter le recours à la médiation et à la conciliation sur la base du rapport rendu en avril 2015 par la mission interministérielle d'évaluation de l'offre de médiation et de conciliation**
- **Permettre aux citoyens de mieux évaluer les possibilités de succès de leurs actions en justice**
    - Dans certains contentieux civils (pension alimentaire, prestation compensatoire, indemnisation du préjudice corporel…) des informations sur les décisions habituellement rendues par les juridictions au niveau national seront mises à disposition du public
    - Au niveau local, des juridictions pilotes ont engagé un partenariat avec les universités afin d'analyser leur jurisprudence. Utiles aux magistrats pour assurer la cohérence de leurs décisions, ces analyses permettront en outre aux avocats et aux citoyens de disposer d'un document facilitant leurs démarches et un éventuel règlement amiable du litige

[En savoir plus](http://gouvernement-ouvert.etalab.gouv.fr/content/fr/consulter-concerter-co-produire/renovation-des-pratiques/engagement-14.html)
 
## Description des résultats :

**Actions** | **Résultats** | **Prochaines étapes** | **Statut**
--- | --- | --- | ---
Ouvrir la justice à la société par la création de conseils de juridiction. | Les conseils de juridiction ont été expérimentés à compter de janvier 2015 dans 3 cours d'appel et 17 TGI. Une circulaire a été adressée aux juridictions expérimentales le 27 février 2015 et un premier comité de pilotage réunissant l'ensemble de ces juridictions a été organisé le 12 mai 2015. Il a donné un retour très positif de ces expérimentations et la décision de généralisation a été prise. Les 20 juridictions ont pu mener des réflexions sur des problématiques transverses, en lien resserré avec les acteurs locaux et en évaluant les résultats sur le terrain. Le [décret 2016-514](https://www.legifrance.gouv.fr/jo_pdf.do?id=JORFTEXT000032459686) du 26 avril 2016 généralise donc à l’ensemble des juridictions (tribunaux de grande instance et cours d’appel) la mise en œuvre du conseil de juridiction suite à l’expérimentation lancée. | Suivre la généralisation des conseils de juridiction. | Substantiel
Faciliter le recours à la médiation et à la conciliation sur la base du rapport rendu en avril 2015 par la mission interministérielle d’évaluation de l’offre de médiation et de conciliation. | Des mesures visant à faciliter le recours à la médiation ont été introduites dans le [décret 2016-514](https://www.legifrance.gouv.fr/jo_pdf.do?id=JORFTEXT000032459686) et devrait entrer en vigueur rapidement afin de permettre le développement de ces modes alternatifs de règlement des litiges. Par ailleurs, le [projet de loi de modernisation de la justice du XXIème siècle](https://www.legifrance.gouv.fr/affichLoiPreparation.do?idDocument=JORFDOLE000030962821&type=general&typeLoi=proj&legislature=14), qui doit être examiné par l'Assemblée nationale dans le courant du mois de mai 2016, contient des dispositions visant à favoriser le développement de la médiation. | Suivre l'examen du projet de loi. | Substantiel
Permettre aux citoyens de mieux évaluer les possibilités de succès de leurs actions en justice. | Afin de permettre aux citoyens de disposer de plus de prévisibilité sur les décisions rendues par les juridictions, le ministère de la Justice a mis en ligne un portail d'information du justiciable, [justice.fr](http://justice.fr/), première étape du projet PORTALIS, de refonte des applications civiles du ministère. Ce portail propose trois simulateurs (pensions alimentaires, aide juridictionnelle, saisie des rémunérations). Par ailleurs, les partenariats avec les universités  se développent au niveau  local afin de faire connaitre et partager la jurisprudence d'une juridiction sur un contentieux déterminé. Enfin, les travaux engagés avec la Cour de cassation afin de permettre le développement de la mise à disposition des décisions de cours d'appel et première instance aux  citoyens - open data des décisions de justice- se poursuivent et permettront à terme de mettre à disposition du public les données judiciaires. | Poursuivre les travaux engagés. | Substantiel

## Une belle histoire : 
 Le portail [justice.fr](http://justice.fr/), ouvert depuis le 12 mai 2016, permet aux citoyens d'accéder aisément aux informations qu'ils souhaitent connaître. A l'occasion de son inauguration, le garde des Sceaux a convié de jeunes entreprises désireuses de fournir des prestations en ligne, lors d'un [Jeudigital organisé à la Chancellerie](http://www.justice.gouv.fr/le-ministere-de-la-justice-10017/jeudigital-28967.html). Retours en vidéo : 
 
 **En savoir plus** | **Liens**
 --- | ---  
Retours en vidéo | [![Video](http://img.youtube.com/vi/No7NT1H_EzQ/0.jpg)](http://www.youtube.com/watch?v=No7NT1H_EzQ)
Compte-rendu | [Compte-rendu jeudigital justice](http://www.economie.gouv.fr/jeudigital-12-edition-ministere-justice)
