# 2.2. Consulation lors de la mise en oeuvre du Plan d'action national

La mise en oeuvre concrète du plan d'action national suppose d'ouvrir aux citoyens des outils de suivi et de participation en ligne et hors ligne. 

## Plusieurs dispositifs sont proposés : 

1. Suivre l'avancement des actions réalisées engagement par engagement, publié dans le présent rapport. Les citoyens peuvent commenter directement ce suivi par l'intermédiaire d'un forum, intégré au sein de chaque page de suivi.

                                       

2.  Envoyer une contribution écrite à l'adresse <gouvernement-ouvert@etalab.gouv.fr>. Les contributions seront rendues publiques. 
                    
                    
3. Suggérer des améliorations dans la méthodologie de consultation et de suivi du Plan d'action national, dans la section **["Contribuer"](contribuer.md)** de ce présent livre : les contributeurs peuvent suggérer des outils et méthodologies de consultation, ou encore toute autre idée qui permette d'améliorer le suivi des engagements (construction d'indicateurs quantitatifs et qualitatifs, formats d'ateliers présentiels, etc.) 

## Le rapport d'autoévaluation sera soumis à consultation publique du 13 au 30 juin 2016 : 
- Sa version finale, publiée en juillet 2016, sera mise à jour suite aux contributions citoyennes.
- Des rencontres en présentiel pourront être organisées durant toute la mise en oeuvre du Plan d'action national et ce jusqu'au rapport d'autoévaluation final prévu pour juillet 2017.
- La liste des organisations et participants à l'ensemble de ces dispositifs sera publiée et régulièrement mise à jour.

## Calendrier (au mois de mai 2016) :

![calendrier de suivi](images/calendrierbis.png)