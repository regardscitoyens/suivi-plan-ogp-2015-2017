# 4. Mise en oeuvre des engagements du Plan d'action national

Cette partie présente de manière [synthétique](tableau-recap.md) et [détaillée](details-engagemements.md) l'avancement des engagements du Plan d'action national.

### Dans la partie synthétique :

Un tableau résume la mise en oeuvre globale des engagements. Pour chaque engagement est inscrit son statut (non démarré, partiel, substantiel, complet) et son état d'avancement par rapport au calendrier fixé.


### Dans la partie détaillée : 
- le détail des institutions porteuses, des enjeux et de la feuille de route est rappelé
- un tableau présente ensuite l'avancement de la mise en oeuvre de chaque action de la feuille de route en plusieurs onglets : les éléments de mise en oeuvre et les prochaines étapes. Chaque action est qualifiée avec un statut : **non démarré**, **partiel**, **substantiel**, **complet**.
- il est enfin présenter un ou plusieurs exemples concrets de réussite de l'engagement
                                    
                                    
                                    
Cette partie "suivi" est le fruit du travail mené par l'administration en charge de la coordination de l'action de la France au sein du PGO (Etalab/SGMAP) avec l'ensemble des institutions porteuses d'engagements et la société civile. Depuis l'adoption du Plan d'action en juillet, Etalab est en lien avec un réseau de porteurs métiers dans l'ensemble de l'administration pour échanger avec eux et accompagner leurs actions d'ouverture, de transparence et de nouvelles collaborations. Ce suivi est ensuite partagé lors de réunions rassemblant les différents ministères, au cours desquelles sont définies les prochaines actions à mettre en oeuvre.

### Une première réunion avec les [administrations concernées](administrations_pan.md) a eu lieu le 13 mai 2016.

