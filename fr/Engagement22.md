# Engagement 22 : Diffuser l'innovation et approfondir la recherche sur le gouvernement ouvert

## Institutions porteuses : 
- Ministère de la Fonction publique
- Secrétariat d'État chargé de la Réforme de l'État et de la Simplification

## Enjeux : 
L'administration doit pouvoir s'allier les apports de l'intelligence collective et développer une culture de l'innovation, levier essentiel de modernisation de l'action publique. La co-construction de l'action publique est cependant une dynamique encore récente, que la recherche appliquée peut contribuer à améliorer.

## Description de l'engagement : 
- **Impulser le développement de l'innovation publique territoriale**
    - Constituer un réseau national « d'accélérateurs publics », soit la création de plateformes territoriales d'innovation, « structure de partage de compétences et de moyens entre les administrations, les élus, les services de l'État, la société civile et les organisations privées d‘un territoire, pour accélérer les projets innovants d'intérêt général ». Des prototypes sont en cours d'installation sur quelques territoires à l'initiative de collectivités et d'acteurs privés
    - Faire du numérique un levier de transformation dans les territoires en organisant des « hackathons territoriaux » sur le modèle de l'offre de service développée par Etalab à destination des collectivités
    - Développer une plateforme web et un réseau social nationaux servant de référence pour les projets et acteurs de l'innovation territoriale
- **Mettre en place un programme de recherche appliquée sur le gouvernement ouvert**
    - En partenariat avec le Centre de Recherches Interdisciplinaires, organiser des évènements collaboratifs et mener des expérimentations sur la mobilisation de l'intelligence collective et des communautés de citoyens pour la création d'innovations publiques, la co-construction de l'action publique et le gouvernement ouvert

[En savoir plus](http://gouvernement-ouvert.etalab.gouv.fr/content/fr/ouvrir-l-administration/ouverture-et-innovation/engagement-22.html)
 
## Description des résultats :

**Actions** | **Résultats** | **Prochaines étapes** | **Statut**
--- | --- | --- | ---
Constituer un réseau national « d’accélérateurs publics », soit la création de plateformes territoriales d’innovation, « structure de partage de compétences et de moyens entre les administrations, les élus, les services de l’État, la société civile et les organisations privées d‘un territoire, pour accélérer les projets innovants d’intérêt général ». Développer une plateforme web et un réseau social nationaux servant de référence pour les projets et acteurs de l'innovation territoriale. | Deux appels à projet PIA ont été lancés en avril 2016, l'un sur les "[communautés professionnelles territoriales](http://www.gouvernement.fr/sites/default/files/contenu/piece-jointe/2016/04/aap_complet_communautes_professionnelles_territoriales_0.pdf)", l'autre sur les ["laboratoires d'innovations territoriales"](http://www.gouvernement.fr/sites/default/files/contenu/piece-jointe/2016/04/aap_complet_laboratoires_innovations.pdf). | Présenter à l'automne les projets de la première vague de juin du PIA. Travailler sur les suites du rapport ["L'innovation au pouvoir. Pour une action publique réinventée au service des territoires"](http://www.action-publique.gouv.fr/files/files/PDF/2015_rapport_innovation_territoriale.pdf) d'Akim Oural. | Partiel
Faire du numérique un levier de transformation dans les territoires en organisant des « hackathons territoriaux » sur le modèle de l’offre de service développée par Etalab à destination des collectivités. | Un [BarCamp sur la transparence de la commande publique](https://www.etalab.gouv.fr/retour-sur-le-barcamp-la-commande-publique-augmentee-par-la-donnee) a été organisé par le secrétariat général pour la modernisation de l'action publique et le Conseil régional de Bretagne dans les locaux de la French Tech de Rennes/Saint-Malo. La FING organise de plus des séminaires [Infolab](http://fing.org/?-Infolab-) pour les collectivités adhérentes (Rennes Métropole, Grand Poitiers, Grand Lyon, Ile-de-France, CD33, etc.) | Accompagner la FING dans la mise en oeuvre des campagnes Infolab. | Partiel
En partenariat avec le Centre de Recherches Interdisciplinaires, organiser des évènements collaboratifs et mener des expérimentations sur la mobilisation de l'intelligence collective et des communautés de citoyens pour la création d'innovations publiques, la co-construction de l'action publique et le gouvernement ouvert | Une convention de partenariat a été signée avec le CNRS pour l'intégration d'une chercheuse embarquée dans l'équipe d'Etalab sur le dossier "gouvernement ouvert". Des cours et un atelier de travail ont eu lieu à Londres le 26 mai 2016 avec NESTA et le Centre de recherches interdisciplinaires. Enfin, des Academic Days sont prévus pendant 3 jours en amont du Sommet mondial 2016 du Partenariat pour un gouvernement ouvert. Organisé par des chercheurs sur le gouvernement ouvert, cet événement met également à contribution le secrétariat d'Etat chargé de l'Enseignement supérieur et de la Recherche. | | Partiel
