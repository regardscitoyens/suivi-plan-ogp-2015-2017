# Engagement 9 : Accroître la transparence sur les négociations commerciales internationales

## Institutions porteuses :
- Ministère de l'Environnement, de l'Energie et de la Mer
- Ministère de l'Économie, de l'Industrie et du Numérique
- Secrétariat d'État chargé du Commerce extérieur, de la Promotion du tourisme et des Français de l'étranger

### Enjeux :
La France est engagée dans de nombreux cycles de négociations commerciales
multilatérales à fort impact économique. **La mise en place d'un dialogue avec la société
civile et la transparence des positions françaises facilitent la compréhension par les
entreprises, les associations et les citoyens des enjeux associés à ces politiques** : services
publics,  préservation  des  préférences  collectives  (normes  sanitaires,  sociales  et
environnementales), diversité culturelle, barrières non tarifaires, accès aux marchés publics,
mécanismes de règlement des différends…


## Description de l'engagement : 
- **Eclairer la société civile sur le contenu des négociations internationales**
    - Les éléments relatifs à chaque nouveau cycle de négociation commerciale internationale seront mis à disposition du public en continu sur la page dédiée du site [diplomatie.gouv.fr](http://diplomatie.gouv.fr/fr/) et en open data sur [data.gouv.fr](https://www.data.gouv.fr/fr/)
    - Des éléments complémentaires, tels que les mandats, pourront être ajoutés selon les décisions de déclassification prises par l'Union européenne
    - Le stock des éléments relatifs aux négociations commerciales internationales passées sera ajouté à ce corpus, comme les mandats de négociation de la Commission européenne rendus publics, les documents de position officiels mis à la disposition du public et le texte des traités de commerce ratifiés et publics
- **Assurer un maximum de publicité aux évaluations et au suivi des accords internationaux**
    - Des études et éléments d'analyse et d'évaluation des accords commerciaux ex-post et ex-ante, qu'ils soient transversaux ou sectoriels, pourront également être ajoutés
    - La représentation nationale pourra être informée grâce à la remise d'un rapport annuel sur les négociations commerciales internationales
    - Tous les comptes rendus du Comité de suivi stratégique des sujets de politique commerciale seront portés à la connaissance du public, ainsi que les rapports annuels sur les négociations commerciales internationales

[En savoir plus](http://gouvernement-ouvert.etalab.gouv.fr/content/fr/rendre-des-comptes/transparence-vie-economique/engagement-9.html)

## Description des résultats :

**Actions** | **Résultats** | **Prochaines étapes** | **Statut**
--- | --- | --- | ---
Eclairer la société civile sur le contenu des négociations internationales. | Le cabinet du secrétariat d’Etat chargé du Commerce extérieur, de la Promotion du tourisme et des Français de l'étranger a publié sur data.gouv.fr une nouvelle vague de [données concernant les négociations commerciales]( https://www.data.gouv.fr/fr/datasets/corpus-de-documents-relatif-aux-negociations-commerciales-internationales-en-cours-ttip-tisa-et-ceta/), régulièrement mises à jour. Deux membres du cabinet sont devenus administrateurs du compte sur data.gouv.fr. | Poursuivre l'effort afin de s’assurer que le flux de données soit bien publié. | Substantiel
Assurer un maximum de publicité aux évaluations et au suivi des accords internationaux. | | | Information non disponible
